// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "CityNode.generated.h"

UCLASS()
class CITYDRAW_API ACityNode : public AActor {
 private:
	GENERATED_BODY()
    int nodeID = 0;
    FVector nodePosition;
    float nodeCurrentHeight;

    UPROPERTY(Category = Meshes, VisibleAnywhere)
    UStaticMeshComponent *nodeMesh;


    UPROPERTY(Category = Materials, EditAnywhere)//initialized in the inspector
    UMaterial *originalNodeMaterial;
    UMaterialInstanceDynamic *nodeMaterialInstance;

    //TSubclassOf<class ACityNode> nodeBlueprint;
    bool adjustingNodeHeight = false;
    void AnimateHeight(const float &offset, const float &deltaTime, const float &speed);
 public:

    int GetNodeID() const { return nodeID; }
    FVector GetNodePosition() const { return nodePosition; }

    void SetNodeColor(const float &r, const float &g, const float &b);
	// Sets default values for this actor's properties
	ACityNode(const class FObjectInitializer& PCIP);

    void InitializeNode(const FVector &pos, const int &id);

    void SetNodeHeightTo(const float &newHeight);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;
};
