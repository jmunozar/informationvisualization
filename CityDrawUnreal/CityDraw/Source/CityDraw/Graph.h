/*
 Created by: Juan Sebastian Munoz Arango
 naruse@gmail.com
 all rights reserved

 Graph class for calculating minimun path for each node
 */
#pragma once

#include "CityNode.h"
#include <limits>
#include "CitySegment.h"

class CITYDRAW_API Graph {
private:
    int numberOfNodes;
    TArray<ACityNode*> nodes;
    TArray<ACitySegment*> segments;

    float** adjacenceMatrix;
public:
    Graph();
	Graph(const TArray<ACityNode*> &_nodes, const TArray<ACitySegment*> &_segments);
	~Graph();

    void CalculateMinimumPath();
    int GetNumberOfNodes() const { return numberOfNodes; }
    float GetValue(const int &i, const int &j) const { return adjacenceMatrix[i][j]; }
};
