// Fill out your copyright notice in the Description page of Project Settings.

#include "CityDraw.h"
#include "CityDrawUIWidget.h"


// Binds all references from the Widget creator to our class so we can change
// them from code.
void UCityDrawUIWidget::InitializeGUIReferences() {
    UWidget* reference = this->GetWidgetFromName(TEXT("NumberOfAgentsValueLabel"));
    bottomLeftText = Cast<UTextBlock>(reference);

    processingAnimationRef = GetWidgetFromName(TEXT("ProcessingAnim"));
    processingAnimationRef->SetVisibility(ESlateVisibility::Hidden);

    canvasHelp = GetWidgetFromName(TEXT("HelpCanvas"));
    canvasHelp->SetVisibility(ESlateVisibility::Visible);
}

void UCityDrawUIWidget::ShowProcessingAnimation(const bool &show) {
    if(show)
        processingAnimationRef->SetVisibility(ESlateVisibility::Visible);
    else
        processingAnimationRef->SetVisibility(ESlateVisibility::Hidden);
}

void UCityDrawUIWidget::ToggleShowHelp() {
    showingHelpUI = !showingHelpUI;
    if(showingHelpUI)
        canvasHelp->SetVisibility(ESlateVisibility::Visible);
    else
        canvasHelp->SetVisibility(ESlateVisibility::Hidden);
}
