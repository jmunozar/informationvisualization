// Fill out your copyright notice in the Description page of Project Settings.
#include "CityDraw.h"
#include "CitySegment.h"

// Sets default values
ACitySegment::ACitySegment(const class FObjectInitializer& PCIP) {
    PrimaryActorTick.bCanEverTick = false;
    segmentMesh = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("SegmentMesh"));
    originalMaterial = PCIP.CreateDefaultSubobject<UMaterial>(this, TEXT("SegmentMaterial"));
    RootComponent = segmentMesh;

    //static ConstructorHelpers::FObjectFinder<UBlueprint>
    //    citySegmentBP(TEXT("Blueprint'/Game/StarterContent/Blueprints/CitySegment_Blueprint.CitySegment_Blueprint'"));

    //static ConstructorHelpers::FObjectFinder<UMaterial> matFinder(TEXT("Material'/Game/ParentSEGMENTMat.ParentSEGMENTMat"));
    //if(matFinder.Succeeded())
    //    segmentParentMaterial = (UMaterial*) matFinder.Object;

    /*    if(citySegmentBP.Object){
        segmentBlueprint = (UClass*)citySegmentBP.Object->GeneratedClass;
        }*/
}

// Called when the game starts or when spawned
void ACitySegment::BeginPlay() {
	Super::BeginPlay();

    // Create the material dynamic instance in __BEGIN_PLAY__ and then using that precreated instance.
    // Also make sure that you are setting parameters on the MID that actually exist in the material.
    // what we do with the material is get the one from the static mesh,
    // create a material and then assign it

    /*segmentMaterialInstance = UMaterialInstanceDynamic::Create(originalMaterial,this);
    if(segmentMaterialInstance->IsValidLowLevel()) {
        segmentMesh->SetMaterial(0, segmentMaterialInstance);
        segmentMaterialInstance->SetVectorParameterValue(FName(TEXT("SegmentColor")),FLinearColor(1,1,0, 1.0));
    } else {
        UE_LOG(LogTemp, Error, TEXT("Not valid material low level!"));
    }*/
}

void ACitySegment::PlaceSegment(ACityNode const* nodeA, ACityNode const* nodeB) {
    FVector segmentPos = (nodeA->GetNodePosition() + nodeB->GetNodePosition())/2;
    FRotator segmentRotation = FRotationMatrix::MakeFromX(segmentPos - nodeB->GetActorLocation()).Rotator();

    startNodeID = nodeA->GetNodeID();
    endNodeID = nodeB->GetNodeID();

    SetActorLocation(segmentPos, false);
    SetActorRotation(segmentRotation);
    if(segmentDistance == 0)
        UE_LOG(LogTemp, Error, TEXT("Seg dist: %f"), segmentDistance);
    SetActorScale3D(FVector(segmentDistance * 0.01f,//0.01 to fit scale of file TODO: think on moving this (0.01)to a define
                            GetActorScale3D().Y * segmentCapacity*segmentCapacity,
                            /*spawnSegment->GetActorScale3D().Z*/0.2f));
}

void ACitySegment::InitializeSegment(const float &_segCapacity, const float &_segMaxSpd,
                                     const float &_segDistance, const float &_maxSegDistanceOverall) {

    segmentCapacity = _segCapacity;
    segmentMaxSpeed = _segMaxSpd;
    segmentDistance = _segDistance;
    segmentMaxDistanceOverall = _maxSegDistanceOverall;
    normalizedDistance = 1 - (segmentDistance/segmentMaxDistanceOverall);

    //CalculateSegmentColor(segmentCapacity);

}

void ACitySegment::ClearSegmentColor() { SetSegmentColor(1,1,1); }
void ACitySegment::PaintSegmentByDistance() { CalculateSegmentColor(normalizedDistance); }
void ACitySegment::PaintSegmentBySpeed() { CalculateSegmentColor(segmentMaxSpeed); }
void ACitySegment::PaintSegmentByCapacityVsSpeedVsDistance() {
    CalculateSegmentColor(normalizedDistance + (segmentMaxSpeed * segmentCapacity));
}

//given a value between [0-1] calculates a color for the segment between
//green and red (green best -> 1), red worst -> 0, yellow -> 0.5
// adapted from http://stackoverflow.com/questions/17525215/calculate-color-values-from-green-to-red
void ACitySegment::CalculateSegmentColor(const float &val) {
    // as the function expects a value between 0 and 1, and red = 0 and green = 120 convert input to appropriate hue value
    float hue = val*100.0 * 1.2 /360;
    // we convert hsl to rgb (saturation 100%, lightness 50%)
    FLinearColor convertedColor = CityUtils::HSLToRGB(hue, 1.0, 0.5f);
    SetSegmentColor(convertedColor.R, convertedColor.G, convertedColor.B);
}

void ACitySegment::SetSegmentColor(const float &r, const float &g, const float &b) {
    segmentMaterialInstance = UMaterialInstanceDynamic::Create(originalMaterial,this);
    if(segmentMaterialInstance) {
        segmentMesh->SetMaterial(0, segmentMaterialInstance);
        segmentMaterialInstance->SetVectorParameterValue(FName(TEXT("SegmentColor")),FLinearColor(r, g, b, 1.0));
    } else {
        UE_LOG(LogTemp, Error, TEXT("Material instance for segment not found!"));
    }
}

// Called every frame
void ACitySegment::Tick(float DeltaTime ) {
	Super::Tick( DeltaTime );

}

