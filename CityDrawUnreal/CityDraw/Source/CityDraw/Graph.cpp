/*
  Created by: Juan Sebastian Munoz Arango
  naruse@gmail.com
  all rights reserved
 */

#include "CityDraw.h"
#include "Graph.h"

Graph::Graph() {}

Graph::Graph(const TArray<ACityNode*> &_nodes, const TArray<ACitySegment*> &_segments) {
    nodes = _nodes;
    segments = _segments;

    numberOfNodes = nodes.Num();
    
    adjacenceMatrix = new float* [numberOfNodes];
    for(int i = 0; i < numberOfNodes; i++) {
        adjacenceMatrix[i] = new float[numberOfNodes];
        for(int j = 0; j < numberOfNodes; j++)
            adjacenceMatrix[i][j] = (i == j) ? 0 : std::numeric_limits<float>::max();
    }

    for(int i = 0; i < segments.Num(); i++) {
        adjacenceMatrix[segments[i]->GetStartNodeID()][segments[i]->GetEndNodeID()] = segments[i]->GetSegmentWeight();
        adjacenceMatrix[segments[i]->GetEndNodeID()][segments[i]->GetStartNodeID()] = segments[i]->GetSegmentWeight();
    }

    UE_LOG(LogTemp, Warning, TEXT("Calculating minimum path for nodes..."));
    CalculateMinimumPath();
    UE_LOG(LogTemp, Warning, TEXT("Finished"));
}


void Graph::CalculateMinimumPath(){ //  |---_ . . . F L O Y D . . . _---|
      for(int k = 0; k < numberOfNodes; k++){
          for(int i = 0; i < numberOfNodes ; i++){
              for(int j = i+1; j < numberOfNodes; j++){
                  if(adjacenceMatrix[i][j] >= adjacenceMatrix[i][k] + adjacenceMatrix[k][j]){
                      adjacenceMatrix[i][j] = adjacenceMatrix[j][i] = adjacenceMatrix[i][k] + adjacenceMatrix[k][j];
                  }
              }
          }
      }
}
Graph::~Graph() {
    /*for(int i = 0; i < numberOfNodes; i++) {
        delete adjacenceMatrix[i];
        adjacenceMatrix[i] = NULL;
    }
    delete adjacenceMatrix;
    adjacenceMatrix = NULL;*/
}
