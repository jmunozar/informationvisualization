// Created by:
// Juan Sebastian Munoz Arango
// naruse@gmail.com
// all rights reserved
//
// This class can be considered the entry point for all the program. Here we
// catch the input for the different visualizations we can have + camera navigation

#include "CityDraw.h"
#include "MainPlayer.h"

/********************* Input Management ***********************/
void AMainPlayer::MoveForward(float axisValue) { movementInput.X = FMath::Clamp<float>(axisValue, -1.0f, 1.0f); }
void AMainPlayer::MoveRight(float axisValue) { movementInput.Y = FMath::Clamp<float>(axisValue, -1.0f, 1.0f); }
void AMainPlayer::MoveUp(float axisValue) { movementInput.Z = FMath::Clamp<float>(axisValue, -1.0f, 1.0f); }
void AMainPlayer::PitchCamera(float axisValue) { cameraInput.Y = axisValue; }
void AMainPlayer::YawCamera(float axisValue) { cameraInput.X = axisValue; }
void AMainPlayer::ZoomIn() { bZoomingIn = true; }
void AMainPlayer::ZoomOut() { bZoomingIn = false; }

void AMainPlayer::ManageCameraInput(const float &deltaTime) {
    if (bZoomingIn) {
        zoomFactor += deltaTime / 0.5f; //Zoom in over half a second
    } else {
        zoomFactor -= deltaTime / 0.25f;//Zoom out over a quarter of a second
    }
    zoomFactor = FMath::Clamp<float>(zoomFactor, 0.0f, 1.0f);
    //Blend our camera's FOV and our SpringArm's length based on ZoomFactor
    ourCamera->FieldOfView = FMath::Lerp<float>(90.0f, 60.0f, zoomFactor);
    ourCameraSpringArm->TargetArmLength = FMath::Lerp<float>(400.0f, 300.0f, zoomFactor);

    {//Rotate our actor's yaw, which will turn our camera because we're attached to it
        FRotator newRotation = GetActorRotation();
        newRotation.Yaw += cameraInput.X;
        SetActorRotation(newRotation);
    }
    {//Rotate our camera's pitch, but limit it so we're always looking downward
        FRotator newRotation = ourCameraSpringArm->GetComponentRotation();
        newRotation.Pitch = FMath::Clamp(newRotation.Pitch + cameraInput.Y, -89.0f, -15.0f);//almost vertical till 15deg
        ourCameraSpringArm->SetWorldRotation(newRotation);
    }
    {//Handle movement based on our "MoveX" and "MoveY" axes
        if (!movementInput.IsZero()) {
            movementInput = movementInput.GetSafeNormal() * 2500.0f;//Scale movement axis values by XX units per second TODO:CHANGE THIS MAGIC NUMBER
            FVector newLocation = GetActorLocation();
            newLocation += GetActorForwardVector() * movementInput.X * deltaTime;
            newLocation += GetActorRightVector() * movementInput.Y * deltaTime;
            newLocation += GetActorUpVector() * movementInput.Z * deltaTime;
            SetActorLocation(newLocation);
        }
    }
}

// Called to bind functionality to input
//to setup input: https://docs.unrealengine.com/latest/INT/Programming/Tutorials/PlayerCamera/3/index.html
void AMainPlayer::SetupPlayerInputComponent(class UInputComponent* InputComponent) {
	Super::SetupPlayerInputComponent(InputComponent);

    check(InputComponent);
    /****** Input binding ******/
    InputComponent->BindAction("ZoomIn", IE_Pressed, this, &AMainPlayer::ZoomIn);
    InputComponent->BindAction("ZoomIn", IE_Released, this, &AMainPlayer::ZoomOut);
    InputComponent->BindAxis("MoveForward", this, &AMainPlayer::MoveForward);
    InputComponent->BindAxis("MoveRight", this, &AMainPlayer::MoveRight);
    InputComponent->BindAxis("MoveUp", this, &AMainPlayer::MoveUp);
    InputComponent->BindAxis("CameraPitch", this, &AMainPlayer::PitchCamera);
    InputComponent->BindAxis("CameraYaw", this, &AMainPlayer::YawCamera);
    /***************************/

    InputComponent->BindAction("ToggleHelp", IE_Pressed, this, &AMainPlayer::ToggleHelp);

    InputComponent->BindAction("IncreaseNumberOfAgents", IE_Pressed, this, &AMainPlayer::IncreaseAgents);
    InputComponent->BindAction("DecreaseNumberOfAgents", IE_Pressed, this, &AMainPlayer::DecreaseAgents);

    InputComponent->BindAction("CalculateAgentCoverage", IE_Pressed, this, &AMainPlayer::CalculateAgentCoverage);

    /**** Segment Coloring binding ****/
    InputComponent->BindAction("ClearSegmentViz", IE_Pressed, this, &AMainPlayer::ClearSegmentsColoring);
    InputComponent->BindAction("PaintSegmentsBySpeed", IE_Pressed, this, &AMainPlayer::PaintSegmentsBySpeed);
    //InputComponent->BindAction("PaintSegmentsBySpeed", IE_Pressed, this, &AMainPlayer::PaintSegmentsByDistance);
    //InputComponent->BindAction("PaintSegmentsBySpeed", IE_Pressed, this, &AMainPlayer::PaintSegmentsByCapVsSpdVsDist);

    /**** Agent coloring binding *****/
    InputComponent->BindAction("PaintAgentsCoveredNodes", IE_Pressed, this, &AMainPlayer::PaintAgentsCoveredNodes);
    InputComponent->BindAction("PaintAgentsReactionTime", IE_Pressed, this, &AMainPlayer::PaintAgentsReactionTime);

}


/** not used **/
void AMainPlayer::PaintSegmentsByDistance() {
    currentVisualizationStr = TEXT("");
    for(int i = 0; i < citySegments.Num(); i++)
        citySegments[i]->PaintSegmentByDistance();
}
void AMainPlayer::PaintSegmentsByCapVsSpdVsDist() {
    currentVisualizationStr = TEXT("");
    for(int i = 0; i < citySegments.Num(); i++)
        citySegments[i]->PaintSegmentByCapacityVsSpeedVsDistance();
}
/************/

/**************************************************************/
/******* Input Calls *******/
void AMainPlayer::ClearSegmentsColoring() {
    currentVisualizationStr = TEXT("");
    for(int i = 0; i < citySegments.Num(); i++)
        citySegments[i]->ClearSegmentColor();
}
void AMainPlayer::PaintSegmentsBySpeed() {
    currentVisualizationStr = TEXT(" Visualizing speed.");
    for(int i = 0; i < citySegments.Num(); i++)
        citySegments[i]->PaintSegmentBySpeed();
}
void AMainPlayer::PaintAgentsCoveredNodes() {
    currentVisualizationStr = agentsInCity.Num() > 1 ? TEXT(" Visualizing agents covered nodes.") : TEXT(" No agents to visualize (0 agents).");
    for(int i = 0; i < agentsInCity.Num(); i++) {
        FLinearColor generatedColor = CityUtils::GenerateColorFromPreset(i);
        agentsInCity[i]->ColorCoveredNodes(generatedColor.R, generatedColor.G, generatedColor.B);
    }
}
void AMainPlayer::PaintAgentsReactionTime() {
    currentVisualizationStr = agentsInCity.Num() > 1 ? TEXT(" Visualizing agents reaction time.") : TEXT(" No agents to visualize (0 agents).");
    for(int i = 0; i < agentsInCity.Num(); i++)
        agentsInCity[i]->ColorCoveredNodesByReactionTime(cityGraph);
}

void AMainPlayer::ToggleHelp() {
    cityDrawGUI->ToggleShowHelp();
}

void AMainPlayer::CalculateAgentCoverage() {
    for(int i = 0; i < cityNodes.Num(); i++) {
        cityNodes[i]->SetNodeColor(1,1,1);
    }
    for(int i = 0; i < agentsInCity.Num(); i++) {
        agentsInCity[i]->Destroy();
    }
    agentsInCity.Empty();
    cityDrawGUI->ShowProcessingAnimation(true);//TODO: make this not block framerate


    TArray<int> agentLocationIndices = CalculateMoveableAgentsLocations(cityGraph, numberOfMoveableAgents);

    SpawnMoveableAgentsInCityMap(cityNodes, agentLocationIndices);
    CalculateNodesPerMoveableAgent();
    float maxNodeHeight = 10;
    for(int i = 0; i < agentsInCity.Num(); i++) {
        agentsInCity[i]->CalculateCoveredNodeHeights(cityGraph, maxNodeHeight);
        agentsInCity[i]->SetAgentColor(1,1,1);//white agents for easier recognition
    }
    cityDrawGUI->ShowProcessingAnimation(false);
}
/***************************/

// Sets default values
AMainPlayer::AMainPlayer() {
	PrimaryActorTick.bCanEverTick = true;

    static ConstructorHelpers::FClassFinder<UCityDrawUIWidget> classFinderUIWidget(TEXT("/Game/StarterContent/Blueprints/CityDrawGUI.CityDrawGUI_C"));
    cityDrawWidgetClass = classFinderUIWidget.Class;

    //Create our components
    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
    ourCameraSpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraSpringArm"));
    ourCameraSpringArm->AttachTo(RootComponent);
    ourCameraSpringArm->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, 50.0f), FRotator(-60.0f, 0.0f, 0.0f));
    ourCameraSpringArm->TargetArmLength = 400.f;
    ourCameraSpringArm->bEnableCameraLag = true;
    ourCameraSpringArm->CameraLagSpeed = 3.0f;
    ourCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("GameCamera"));
    ourCamera->AttachTo(ourCameraSpringArm, USpringArmComponent::SocketName);

    //Take control of the default Player
    AutoPossessPlayer = EAutoReceiveInput::Player0;
}

// Called when the game starts or when spawned
void AMainPlayer::BeginPlay(){
	Super::BeginPlay();

    cityDrawGUI = CreateWidget<UCityDrawUIWidget>(GetWorld(), cityDrawWidgetClass);
    if(!cityDrawGUI) {
        UE_LOG(LogTemp, Warning, TEXT("Couldnt reference GUI for city draw.!!"));
    } else {
        cityDrawGUI->AddToViewport();
        cityDrawGUI->InitializeGUIReferences();
    }

    ReadAndLoadCityNodes();

    ReadAndLoadCitySegments();

    cityGraph = Graph(cityNodes, citySegments);
}

// Called every frame
void AMainPlayer::Tick(float DeltaTime ) {
	Super::Tick(DeltaTime);
    OnGUI();
    ManageCameraInput(DeltaTime);
}


//called from InputManager.

void AMainPlayer::OnGUI() {
    cityDrawGUI->SetBottomLeftText(FString::FromInt(numberOfMoveableAgents) + currentVisualizationStr);
}

void AMainPlayer::CalculateNodesPerMoveableAgent() {
    float menor = std::numeric_limits<float>::max();
    int pos = 0;
    for(int j = 0; j < cityGraph.GetNumberOfNodes(); j++) {
        for(int k = 0; k < agentsInCity.Num(); k++) {
            if((GetDistance(agentsInCity[k]->GetAgentPositionNodeID(),j) <= menor) &&
               (agentsInCity[k]->GetAgentPositionNodeID() != j)) {
                menor = GetDistance(agentsInCity[k]->GetAgentPositionNodeID(), j);
                pos = k;
            } else {
                if (agentsInCity[k]->GetAgentPositionNodeID() == j) {
                    pos = k;
                    break;
                }
            }
        }
        menor = std::numeric_limits<float>::max();
        agentsInCity[pos]->AddCoveredNode(cityNodes[j]);
    }
    for(int i = 0; i < agentsInCity.Num(); i++) {

        FLinearColor generatedColor = CityUtils::GenerateColorFromPreset(i);
        agentsInCity[i]->ColorCoveredNodes(generatedColor.R, generatedColor.G, generatedColor.B);
    }

    /*float sum = 0;
    for (unsigned int i = 0; i < agentsInCity.Num(); i++) {
        sum += agentsInCity.at(i).numOfNodes();
        }*/
    //cout << " total nodos: " << sum << endl;
    //cout << "PROMEDIO GENERAL: " << promedioGeneral(numAmb) << endl;
}

float AMainPlayer::AverageDistance(AMoveableAgent const* moveableAgent) {
    float cub = 0;
    for (int i = 0; i < moveableAgent->GetCoveredNodes().Num(); i++) {
        cub += GetDistance(moveableAgent->GetAgentPositionNodeID(),moveableAgent->GetCoveredNodes()[i]->GetNodeID());
    }
    return cub/(float)moveableAgent->GetCoveredNodes().Num();
}

void AMainPlayer::SpawnMoveableAgentsInCityMap(const TArray<ACityNode*> &nodes, const TArray<int> &agentLocationIndices) {
    TSubclassOf<class UObject> bpMoveableAgent = CityUtils::FindOrLoadBluePrintClass(TEXT("/Game/StarterContent/BluePrints/MoveableAgent_Blueprint"));
    for(int i = 0; i < agentLocationIndices.Num(); i++) {
        AMoveableAgent *spawnMoveableAgent = CityUtils::SpawnBP<AMoveableAgent>(GetWorld(), bpMoveableAgent, FVector(0), FRotator(0));
        spawnMoveableAgent->SetPosition(nodes[agentLocationIndices[i]]->GetNodePosition(), nodes[agentLocationIndices[i]]->GetNodeID());
        agentsInCity.Add(spawnMoveableAgent);
    }
}

void AMainPlayer::ReadAndLoadCityNodes() {
    TArray<TArray<FString>> data = CityUtils::ReadFile("Content/CityData/GeneratedCityNodes.txt");
    TSubclassOf<class UObject> bpCityNode = CityUtils::FindOrLoadBluePrintClass(TEXT("/Game/StarterContent/BluePrints/CityNode1_Blueprint"));
    int32 numberOfNodes = FCString::Atoi(*data[0][0]);
    UE_LOG(LogTemp, Warning, TEXT("NUMBER OF SEGMENTS: %i"), numberOfNodes);
    for(int i = 1; i < data.Num(); i++) {//START FROM 1 as [0] contains the number of nodes the file has
        int32 nodeID = FCString::Atoi(*data[i][0]);//<-- node ID
        float nodePosX = FCString::Atof(*data[i][1]);//<-- positionX
        float nodePosY = FCString::Atof(*data[i][2]);//<-- positionY

        //UE_LOG(LogTemp, Warning, TEXT("%i %f %f"), nodeID, nodePosX, nodePosY);
        FVector position = FVector(nodePosX, nodePosY, 0);
        ACityNode *spawnCityNode = CityUtils::SpawnBP<ACityNode>(GetWorld(), bpCityNode, position, FRotator(0));
        spawnCityNode->InitializeNode(FVector(nodePosX, nodePosY, 0), nodeID);
        cityNodes.Add(spawnCityNode);
    }
}

void AMainPlayer::ReadAndLoadCitySegments() {
    TArray<TArray<FString>> data = CityUtils::ReadFile("Content/CityData/GeneratedCitySegments.txt");
    TSubclassOf<class UObject> bpCitySegment = CityUtils::FindOrLoadBluePrintClass(TEXT("/Game/StarterContent/BluePrints/CitySegment_Blueprint"));

    int32 numberOfSegments = FCString::Atoi(*data[0][0]);
    float largestSegmentDistance = FCString::Atof(*data[0][1]);
    UE_LOG(LogTemp, Warning, TEXT("largest distance!: %f"), largestSegmentDistance);
    for(int i = 1; i < data.Num(); i++) {//START FROM 1, as [0] is number of segments and max segment distance
        int32 startNode = FCString::Atoi(*data[i][0]);//<-- start node ID
        int32 endNode = FCString::Atoi(*data[i][1]);//<-- end node ID
        float segmentDistance = FCString::Atof(*data[i][2]);//<-- distance from start to end
        float segmentCapacity = FCString::Atof(*data[i][3]);//<-- how wide is the segment
        float segmentMaxSpeed = FCString::Atof(*data[i][4]);//<-- Segment Max Speed

        if(segmentDistance == 0)
            UE_LOG(LogTemp, Warning, TEXT("DISTSTR: %s VALUE: %f"),*data[i][2], segmentDistance);

        ACitySegment *spawnSegment = CityUtils::SpawnBP<ACitySegment>(GetWorld(), bpCitySegment, FVector(0,0,0), FRotator(0));

        spawnSegment->GetSegmentMesh()->SetMobility(EComponentMobility::Movable);//set the mesh movable to modify it
            spawnSegment->InitializeSegment(segmentCapacity, segmentMaxSpeed, segmentDistance, largestSegmentDistance);
            spawnSegment->PlaceSegment(cityNodes[startNode], cityNodes[endNode]);
        spawnSegment->GetSegmentMesh()->SetMobility(EComponentMobility::Static);//set the mesh static back for perf purposes
        citySegments.Add(spawnSegment);
    }
}

//returns a TArray<int> of indices where the moveable locations should be placed
TArray<int> AMainPlayer::CalculateMoveableAgentsLocations(const Graph &g,const int &numberOfAgents) {
    TArray<int> optimumLocations;

    float sum = 0;
    float smallestSum = std::numeric_limits<float>::max();
    int centerOfGraphIndex = -1;

    //add each row of the adjacencematrix in the graph and find the lowest
    //weight index of all nodes
    for(int i = 0; i < g.GetNumberOfNodes(); i++) {
        sum = 0;
        for(int j = 0; j < g.GetNumberOfNodes(); j++) {
            sum += g.GetValue(i, j);
        }
        if(sum < smallestSum) {
            smallestSum = sum;
            centerOfGraphIndex = i;
        }
    }
    UE_LOG(LogTemp, Warning, TEXT("Center of graph Index = : %i, smallest weight sum: %f"), centerOfGraphIndex, smallestSum);

    if(numberOfAgents == 1) {//trivial case
        optimumLocations.Add(centerOfGraphIndex);
        return optimumLocations;
    } else {
        TArray<float> centerOfGraphWeights;

        for(int i = 0; i < g.GetNumberOfNodes(); i++)
            centerOfGraphWeights.Add(g.GetValue(centerOfGraphIndex, i));

        TArray<float> sortedCenterOfGraphWeights(centerOfGraphWeights);
        sortedCenterOfGraphWeights.HeapSort();
        //for(int i = 0; i < sortedCenterOfGraphWeights.Num(); i++)
        //    UE_LOG(LogTemp, Warning, TEXT("%f ------ %f"), sortedCenterOfGraphWeights[i], centerOfGraphWeights[i]);

        //TODO: Check that the numberOfAgents is smaller or eq than the # of nodes
        for(int i = 0; i < numberOfAgents; i++) {
            float closestWeightToFind = sortedCenterOfGraphWeights[sortedCenterOfGraphWeights.Num()-1-i] / 2;

            /************** TODO: Change this to a less horrible way *************/
            FindAndFlagClosestWeight(closestWeightToFind, centerOfGraphWeights);

            for(int j = 0; j < centerOfGraphWeights.Num(); j++) {
                if(centerOfGraphWeights[j] == -88) {//TODO: <--Change this magic number!
                    optimumLocations.Add(j);
                    UE_LOG(LogTemp, Warning, TEXT("MoveableAgent in node: %i"), j);
                    centerOfGraphWeights[j] = -1;//also change this magic number which is synced
                                                 //with FindAndFlagClosestWeight(,)
                    break;
                }

            }
            /*********************************************************************/
        }
        return optimumLocations;
    }
}

//Finds in an arrray the closest value to val, and marks that position in the
//weights array with a value
void AMainPlayer::FindAndFlagClosestWeight(const float& val, TArray<float> &weights) {
    TArray<float> distances;
    for(int i = 0; i < weights.Num(); i++) {
        distances.Add(fabs(val-weights[i]));
    }
    float smallestDistance = std::numeric_limits<float>::max();
    int smallestIndex = 0;
    for(int i = 0; i < distances.Num(); i++) {
        if((distances[i] < smallestDistance) && (weights[i] != -1)) {
            smallestIndex = i;
            smallestDistance = distances[i];
        }
    }
    weights[smallestIndex] = -88;//<-- TODO: Change this to a less of a magic number!
}



/*TArray<int> AMainPlayer::CalculateMoveableAgentsLocations(const Graph &g,const int &numberOfAgents) {
    TArray<int> optimumLocations;


    if(numberOfAgents == 1) {//trivial case
        float sum = 0;
        float smallestSum = std::numeric_limits<float>::max();
        int centerOfGraphIndex = -1;
        //add each row of the adjacencematrix in the graph and find the lowest added distance
        for(int i = 0; i < g.GetNumberOfNodes(); i++) {
            sum = 0;
            for(int j = 0; j < g.GetNumberOfNodes(); j++) {
                sum += g.GetValue(i, j);
            }
            if(sum < smallestSum) {
                smallestSum = sum;
                centerOfGraphIndex = i;
            }
        }
        UE_LOG(LogTemp, Warning, TEXT("Center of graph Index = : %i, smallest weight sum: %f"), centerOfGraphIndex, smallestSum);
        optimumLocations.Add(centerOfGraphIndex);
        return optimumLocations;
    } else {//we have more than 1 agent to place.
        // 1. find node with largest added distance
        float sum = 0;
        float largestSum = -1;
        int agentLocationIndex = -1;
        for(int i = 0; i < g.GetNumberOfNodes(); i++) {
            sum = 0;
            for(int j = 0; j < g.GetNumberOfNodes(); j++) {
                sum += g.GetValue(i, j);
            }
            if(sum > largestSum) {
                largestSum = sum;
                agentLocationIndex = i;
            }
        }
        optimumLocations.Add(agentLocationIndex);

        for(int i = 1; i < numberOfAgents; i++) {

            for(int j = 0; j < g.GetNumberOfNodes(); j++) {//para cada nodo g...


                for(int k = 0; k < optimumLocations.Num(); k++) {



                }
            }
        }
        return optimumLocations;
    }
}
*/
