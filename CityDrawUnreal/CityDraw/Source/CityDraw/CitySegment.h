// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "CityNode.h"
#include "CityUtils.h"
#include "CitySegment.generated.h"

UCLASS()
class CITYDRAW_API ACitySegment : public AActor {
	GENERATED_BODY()

    UPROPERTY(Category = Meshes, VisibleAnywhere)
    UStaticMeshComponent *segmentMesh;//initialized in the inspector

    UPROPERTY(Category = Materials, EditAnywhere)//initialized in the inspector
    UMaterial *originalMaterial;
    UMaterialInstanceDynamic *segmentMaterialInstance;

    int startNodeID;
    int endNodeID;

    /***** values range [0 - 1], 0 worst, 1 best *****/
    float segmentCapacity;
    float segmentMaxSpeed;
    float normalizedDistance;
    /*************************************************/
    float segmentDistance;
    float segmentMaxDistanceOverall;

    void SetSegmentColor(const float &r, const float &g, const float &b);
    void CalculateSegmentColor(const float &val);
 public:
    // Sets default values for this actor's properties
	ACitySegment(const class FObjectInitializer& PCIP);

    UStaticMeshComponent* GetSegmentMesh() { return segmentMesh; }

    int GetStartNodeID() const { return startNodeID; }
    int GetEndNodeID() const { return endNodeID; }

    void PaintSegmentByDistance();
    void PaintSegmentBySpeed();
    void PaintSegmentByCapacityVsSpeedVsDistance();
    void ClearSegmentColor();

    float GetSegmentWeight() const { return segmentDistance; } //<--TODO: Change this to a better weight

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

    void PlaceSegment(ACityNode const* nodeA, ACityNode const* nodeB);

    void InitializeSegment(const float &segCapacity, const float& segMaxSpd, const float& segDist, const float& maxSegDistOverall);
};
