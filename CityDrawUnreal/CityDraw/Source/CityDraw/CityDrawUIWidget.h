/*
 Created by:
 Juan Sebastian Munoz Arango
 naruse@gmail.com
 all rights reserved
 */
#pragma once

#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/UMG.h"
#include "CityDrawUIWidget.generated.h"

/**
 * https://forums.unrealengine.com/showthread.php?72290-C-with-UMG-HUD
 */
UCLASS()
class CITYDRAW_API UCityDrawUIWidget : public UUserWidget {
 private:
	GENERATED_BODY()
    UWidget* processingAnimationRef;
    UTextBlock* bottomLeftText;

    bool showingHelpUI = true;
    UWidget* canvasHelp;
 public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "My New User Widget")
    FString MyNewWidgetName;

    void InitializeGUIReferences();

    void SetBottomLeftText(const FString &fStr) { bottomLeftText->SetText(FText::FromString(fStr)); }
    void ShowProcessingAnimation(const bool &val);
    void ToggleShowHelp();
};
