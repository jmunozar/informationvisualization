// Fill out your copyright notice in the Description page of Project Settings.

#include "CityDraw.h"
#include "CityNode.h"


// Sets default values
ACityNode::ACityNode(const class FObjectInitializer& PCIP) : Super(PCIP) {
	PrimaryActorTick.bCanEverTick = true;
    nodeMesh = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("CityNodeMesh"));
    originalNodeMaterial = PCIP.CreateDefaultSubobject<UMaterial>(this, TEXT("NodeMaterial"));
    RootComponent = nodeMesh;
}

// Called when the game starts or when spawned
void ACityNode::BeginPlay() {
	Super::BeginPlay();

}

void ACityNode::InitializeNode(const FVector &pos, const int &id) {
    nodeCurrentHeight = 1;//node's default height
    nodeID = id;
    nodePosition = pos;
    SetActorLocation(nodePosition);
}

void ACityNode::SetNodeColor(const float &r, const float &g, const float &b) {
    nodeMaterialInstance = UMaterialInstanceDynamic::Create(originalNodeMaterial,this);
    if(nodeMaterialInstance) {
        nodeMesh->SetMaterial(0, nodeMaterialInstance);
        nodeMaterialInstance->SetVectorParameterValue(FName(TEXT("NodeColor")),FLinearColor(r, g, b, 1.0));
    } else {
        UE_LOG(LogTemp, Error, TEXT("Material instance for node not found!"));
    }
}

// Called every frame
void ACityNode::Tick(float DeltaTime) {
	Super::Tick( DeltaTime );
    if(adjustingNodeHeight) {
        AnimateHeight(0.25, DeltaTime, 3);
    }

}

void ACityNode::AnimateHeight(const float &offset, const float &deltaTime, const float &speed) {
    float currentScale = GetActorScale3D().Z;
    if(currentScale < nodeCurrentHeight-offset)
        SetActorScale3D(FVector(1, 1, currentScale + deltaTime*speed));
    else if(currentScale > nodeCurrentHeight+offset)
        SetActorScale3D(FVector(1, 1, currentScale - deltaTime*speed));
    else
        adjustingNodeHeight = false;
}


//Sets the height to the agent to this value
void ACityNode::SetNodeHeightTo(const float &newHeight) {
    adjustingNodeHeight = true;
    nodeCurrentHeight = newHeight;
}
