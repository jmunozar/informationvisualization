// Created by:
// Juan Sebastian Munoz Arango
// naruse@gmail.com
// all rights reserved
//
// This class can be considered the entry point for all the program. Here we
// catch the input for the different visualizations we can have + camera navigation

#pragma once

#include "GameFramework/Pawn.h"
#include "CityUtils.h"
#include "CityNode.h"
#include "CitySegment.h"
#include "Graph.h"
#include "MoveableAgent.h"
#include "CityDrawUIWidget.h"
#include <limits>

//#include "
#include "MainPlayer.generated.h"

UCLASS()
class CITYDRAW_API AMainPlayer : public APawn {
 private:
    GENERATED_BODY()
    /******** UI *********/
    //UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Player, HUD and UI")
    TSubclassOf<class UCityDrawUIWidget> cityDrawWidgetClass;
	//UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Player, HUD and UI")
	class UCityDrawUIWidget* cityDrawGUI;// The instance of the players UI Widget
    /*********************/

    FString currentVisualizationStr = "";//used OnGUI to know which visualization is going on

    /********* Camera movement Input **********/
    FVector movementInput;
    FVector2D cameraInput;
    float zoomFactor;
    bool bZoomingIn;

    void MoveForward(float axisValue);
    void MoveRight(float axisValue);
    void MoveUp(float axisValue);
    void PitchCamera(float axisValue);
    void YawCamera(float axisValue);
    void ZoomIn();
    void ZoomOut();
    void ManageCameraInput(const float &deltaTime);

    UPROPERTY(EditAnywhere)
    USpringArmComponent* ourCameraSpringArm;
    UCameraComponent* ourCamera;
    /******************************************/

    TArray<ACityNode*> cityNodes;
    TArray<ACitySegment*> citySegments;
    TArray<AMoveableAgent*> agentsInCity;

    Graph cityGraph;

    void ReadAndLoadCityNodes();
    void ReadAndLoadCitySegments();
    void SpawnMoveableAgentsInCityMap(const TArray<ACityNode*> &nodes, const TArray<int> &agentLocationIndices);

    float GetDistance(const int &nodeAID, const int &nodeBID) { return cityGraph.GetValue(nodeAID, nodeBID); }
    void CalculateNodesPerMoveableAgent();

    float AverageDistance(AMoveableAgent const* a);

    void OnGUI();

    void CalculateAgentCoverage();

    int numberOfMoveableAgents = 1;
    const int maxNumAgents = 20;
    const int minNumAgents = 1;
    void IncreaseAgents() {
        numberOfMoveableAgents = (numberOfMoveableAgents < maxNumAgents) ? numberOfMoveableAgents+1 : numberOfMoveableAgents;
    }
    void DecreaseAgents() {
        numberOfMoveableAgents = (numberOfMoveableAgents > minNumAgents) ? numberOfMoveableAgents-1 : numberOfMoveableAgents;
    }

    void ClearSegmentsColoring();
    void PaintSegmentsBySpeed();


    /**** not used ***/
    void PaintSegmentsByDistance();
    void PaintSegmentsByCapVsSpdVsDist();
    /***** not used *****/

    void PaintAgentsCoveredNodes();
    void PaintAgentsReactionTime();

    void ToggleHelp();
 public:

	AMainPlayer();

	virtual void BeginPlay() override;

	virtual void Tick( float DeltaSeconds ) override;

    virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

    TArray<int> CalculateMoveableAgentsLocations(const Graph &g, const int &numberOfMoveableAgents);

    void FindAndFlagClosestWeight(const float& val, TArray<float> &weights);
};
