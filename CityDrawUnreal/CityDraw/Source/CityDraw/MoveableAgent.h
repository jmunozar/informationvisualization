/*
 Created by: Juan Sebastian Munoz Arango
 naruse@gmail.com
 all rights reserved

 This class represents agents that are placed in the map that have covered nodes.
 */

#pragma once

#include "GameFramework/Actor.h"
#include "CityNode.h"
#include "Graph.h"
#include "MoveableAgent.generated.h"

UCLASS()
class CITYDRAW_API AMoveableAgent : public AActor {
private:
	GENERATED_BODY()

    UPROPERTY(Category = Meshes, VisibleAnywhere)
    UStaticMeshComponent *moveableAgentMesh;

    UPROPERTY(Category = Materials, EditAnywhere)//initialized in the inspector
    UMaterial *originalAgentMaterial;
    UMaterialInstanceDynamic *agentMaterialInstance;

    FVector agentPosition;
    int agentPositionNodeID;
    TArray<ACityNode*> nodesCovered;

    float largestDistanceToCoveredNodes = 0;

    void CalculateLargestDistanceToCoveredNodes(const Graph &cityGraph);
public:
	// Sets default values for this actor's properties
	AMoveableAgent(const class FObjectInitializer& PCIP);

    void SetPosition(const FVector& agentPos, const int &agentPosNodeID);
    void AddCoveredNode(ACityNode* nodeToAdd);
    TArray<ACityNode*> GetCoveredNodes() const { return nodesCovered; }

    int GetAgentPositionNodeID() const { return agentPositionNodeID; }

    void ColorCoveredNodes(const float& r, const float &g, const float &b);
    void ColorCoveredNodesByReactionTime(const Graph &cityGraph);

    // Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

    void SetAgentColor(const float &r, const float &g, const float &b);

    void CalculateCoveredNodeHeights(const Graph &cityGraph, const float &maxNodeHeight);

};
