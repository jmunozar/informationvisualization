// Fill out your copyright notice in the Description page of Project Settings.

#include "CityDraw.h"
#include "CityUtils.h"

//CityUtils::CityUtils() {
//}

//CityUtils::~CityUtils() {
//}

//relativePath has to contain also the name of the file
//Returns an array of arrays of strings containing each line separated by space.
TArray<TArray<FString>> CityUtils::ReadFile(FString relativePath) {
    TArray<TArray<FString>> extractedDataFromFile;

    const FString lineDelimiter = "\n";//this is to split each line in the file
    const FString dataDelimiterForEachLine = " ";

    FString gameDir = FPaths::GameDir();
    FString completeFilePath = gameDir + relativePath;
    FString fileRawData = "";
    TArray<FString> lines;//contains an array of strings that contains each line from the file
    FFileHelper::LoadFileToString(fileRawData, *completeFilePath);//gather the whole file as a FString and save it to lines TArray
    int32 numberOfLinesInFile = fileRawData.ParseIntoArray(lines, *lineDelimiter, true);


    for(int i = 0; i < numberOfLinesInFile; i++) {
        TArray<FString> dataInLine;
        int32 numberOfDataInLine = lines[i].ParseIntoArray(dataInLine, *dataDelimiterForEachLine, true);
        extractedDataFromFile.Add(dataInLine);
    }

    /*for(int i = 0; i < extractedDataFromFile.Num(); i++) {
        FString lineData = "";
        for(int j = 0; j < extractedDataFromFile[i].Num(); j++) {
            lineData += "[" + extractedDataFromFile[i][j] + "]";
        }
        UE_LOG(LogTemp, Warning, TEXT("%s"), *lineData);
        }*/
    return  extractedDataFromFile;
}

// finds or loads a blueprint class. Needed to create BP based objects in C++.
TSubclassOf<class UObject> CityUtils::FindOrLoadBluePrintClass(const TCHAR* path) {
     /* usage:
     TSubclassOf<class UObject> BP_Item3DActor= FindOrLoadBluePrintClass(TEXT("/Game/FirstPerson/Blueprints/BP_Item3DActor"));

     AItem3DActor* t = Common::world->SpawnActor<AItem3DActor>(BP_Item3DActor, wPos, SpawnRotation, SpawnParams);
     // 't' actually points to a BP class derived from C++ class 'AItem3DActor'
     */
    UObject* something = StaticLoadObject(UObject::StaticClass(), nullptr, path);
    UBlueprint* bp = Cast<UBlueprint>(something);
    TSubclassOf<class UObject> myItemBlueprint;
    myItemBlueprint = (UClass*) bp->GeneratedClass;

    return myItemBlueprint;
}

void CityUtils::MakeActorLookAt(AActor *actorToRotate, const AActor &targetActor) {
    /* FVector targetLocationPosition = targetActor.GetActorLocation();
    FVector actorToRotatePosition = actorToRotate->GetActorLocation();
    actorToRotate->RelativeRotation = FRotationMatrix::MakeFromX(targetLocationPosition - actorToRotatePosition).Rotator();*/
}


FLinearColor CityUtils::HSLToRGB(const double &h, const double &s, const double &l) {
    float r, g, b;
    if (s == 0.0f)
        r = g = b = l;
    else {
        float q = l < 0.5f ? l * (1.0f + s) : l + s - l * s;
        float p = 2.0f * l - q;
        r = HueToRGB(p, q, h + 1.0f / 3.0f);
        g = HueToRGB(p, q, h);
        b = HueToRGB(p, q, h - 1.0f / 3.0f);
    }
    FLinearColor rgbResult = FVector(r, g, b);
    return rgbResult;
}
// Helper for HSLToRGB
float CityUtils::HueToRGB(const double &p, const double &q, const double &_t) {
    float t = _t;
    if (t < 0.0f) t += 1.0f;
    if (t > 1.0f) t -= 1.0f;
    if (t < 1.0f / 6.0f) return p + (q - p) * 6.0f * t;
    if (t < 1.0f / 2.0f) return q;
    if (t < 2.0f / 3.0f) return p + (q - p) * (2.0f / 3.0f - t) * 6.0f;
    return p;
}


//genereates a color from an array. generated colors are different enough
//(Think on how to generate distant colors programatically
// check -->
//http://martin.ankerl.com/2009/12/09/how-to-create-random-colors-programmatically/
// and -->
//http://devmag.org.za/2012/07/29/how-to-choose-colours-procedurally-algorithms/
//(where it says selecting from a gradient)
FLinearColor CityUtils::GenerateColorFromPreset(const int &i) {
    //Took from: https://bl.ocks.org/mbostock/5577023
    //float offset = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
    //float hue = offset + (int)(0.618033988749895f * i) % 1;//golden ratio
    //float hue = (float)i * 0.618033988749895f - floor(i * 0.618033988749895f);//golden ratio
    //UE_LOG(LogTemp, Warning, TEXT("rand color: %f, hue--> %f"), offset, hue);
    //FLinearColor generatedColor = FLinearColor::FGetHSV(hue*100.0, 99, 99);//CityUtils::HSLToRGB(hue*100.0, 0.5, 0.95);
    const int colorsLength = 12;
    FLinearColor colors[colorsLength] {
        FLinearColor(0.65, 0.81, 0.89), FLinearColor(0.69, 0.35, 0.16), FLinearColor(0.89, 0.10, 0.11),
        FLinearColor(0.20, 0.63, 0.17), FLinearColor(0.98, 0.60, 0.60), FLinearColor(0.70, 0.87, 0.54),
        FLinearColor(0.99, 0.75, 0.44), FLinearColor(1.00, 0.50, 0.00), FLinearColor(0.79, 0.70, 0.84),
        FLinearColor(0.42, 0.24, 0.60), FLinearColor(1.00, 1.00, 0.60), FLinearColor(0.12, 0.47, 0.71)
    };
    return colors[i%colorsLength];
}
