#include "CityDraw.h"
#include "MoveableAgent.h"

// Sets default values
AMoveableAgent::AMoveableAgent(const class FObjectInitializer& PCIP) {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    moveableAgentMesh = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("MoveableAgentMesh"));
    originalAgentMaterial = PCIP.CreateDefaultSubobject<UMaterial>(this, TEXT("SegmentMaterial"));
    RootComponent = moveableAgentMesh;
}

// Called when the game starts or when spawned
void AMoveableAgent::BeginPlay() {
	Super::BeginPlay();
}

// Called every frame
void AMoveableAgent::Tick(float DeltaTime) {
	Super::Tick( DeltaTime );
    float realtimeSeconds = UGameplayStatics::GetRealTimeSeconds(GetWorld());
    FVector agentPos = GetActorLocation() + FVector(0, 0, sin(realtimeSeconds*5)*10);
    SetActorLocation(agentPos);
}

void AMoveableAgent::SetPosition(const FVector &agentPos, const int &_agentPosNodeID) {
    agentPosition = agentPos;
    SetActorLocation(agentPosition + FVector(0,0,100));
    agentPositionNodeID = _agentPosNodeID;
}

void AMoveableAgent::AddCoveredNode(ACityNode* nodeToAdd) {
    nodesCovered.Add(nodeToAdd);
}

void AMoveableAgent::ColorCoveredNodes(const float &r, const float &g, const float &b) {
    for(int i = 0; i < nodesCovered.Num(); i++) {
        nodesCovered[i]->SetNodeColor(r, g, b);
    }
}
void AMoveableAgent::ColorCoveredNodesByReactionTime(const Graph &cityGraph) {
    CalculateLargestDistanceToCoveredNodes(cityGraph);
    for(int i = 0; i < nodesCovered.Num(); i++) {
        // as the function expects a value between 0 and 1, and red = 0 and green = 120 convert input to appropriate hue value
        float normalizedDistance = 1 - (cityGraph.GetValue(agentPositionNodeID, nodesCovered[i]->GetNodeID()) / largestDistanceToCoveredNodes);
        float hue = normalizedDistance*100.0 * 1.2 /360;
        // we convert hsl to rgb (saturation 100%, lightness 50%)
        FLinearColor convertedColor = CityUtils::HSLToRGB(hue, 1.0, 0.5f);
        nodesCovered[i]->SetNodeColor(convertedColor.R, convertedColor.G, convertedColor.B);
        //nodesCovered[i]->SetNodeColor(normalizedDistance, normalizedDistance, normalizedDistance);
    }
}


void AMoveableAgent::SetAgentColor(const float &r, const float &g, const float &b) {
    agentMaterialInstance = UMaterialInstanceDynamic::Create(originalAgentMaterial,this);
    if(agentMaterialInstance) {
        moveableAgentMesh->SetMaterial(0, agentMaterialInstance);
        agentMaterialInstance->SetVectorParameterValue(FName(TEXT("AgentColor")),FLinearColor(r, g, b, 1.0));
    } else {
        UE_LOG(LogTemp, Error, TEXT("Material instance for agent not found!"));
    }
}

void AMoveableAgent::CalculateCoveredNodeHeights(const Graph &cityGraph, const float &maxNodeHeight) {
    CalculateLargestDistanceToCoveredNodes(cityGraph);
    float avgDistance = 0;
    for(int i = 0; i < nodesCovered.Num(); i++) {
        float distanceToNode = cityGraph.GetValue(agentPositionNodeID, nodesCovered[i]->GetNodeID());
        avgDistance += distanceToNode;
    }
    if(nodesCovered.Num() > 1)
        avgDistance = avgDistance/nodesCovered.Num();
    for(int i = 0; i < nodesCovered.Num(); i++) {//calculate nodes height
            float nodeHeight = cityGraph.GetValue(agentPositionNodeID, nodesCovered[i]->GetNodeID())/largestDistanceToCoveredNodes;
            nodesCovered[i]->SetNodeHeightTo(nodeHeight * maxNodeHeight);
        }
    UE_LOG(LogTemp, Warning, TEXT("Node ID: %i covered nodes: %i AvgDistance: %f"), agentPositionNodeID, nodesCovered.Num(), avgDistance);
}

void AMoveableAgent::CalculateLargestDistanceToCoveredNodes(const Graph &cityGraph) {
    for(int i = 0; i < nodesCovered.Num(); i++) {
        float distanceToNode = cityGraph.GetValue(agentPositionNodeID, nodesCovered[i]->GetNodeID());
        if(distanceToNode > largestDistanceToCoveredNodes)
            largestDistanceToCoveredNodes = distanceToNode;
    }
}
