// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 *
 */
class CITYDRAW_API CityUtils {
 private:
    //helper function for HSL
    static float HueToRGB(const double &p, const double &q, const double &t);
 public:
    static TArray<TArray<FString>> ReadFile(FString relativePath);

    /** ESpawnActorCollisionHandlingMethod options:
    Fall back to default settings.
	-->ESpawnActorCollisionHandlingMethod::Undefined
	** Actor will spawn in desired location, regardless of collisions. *
	-->ESpawnActorCollisionHandlingMethod::AlwaysSpawn
	**Actor will try to find nearby non-colliding location(based on shpe
    cmpnents), but will always spawn even if one cant be found.*
	-->ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn
	** Actor will try to find a nearby non-colliding location (based on
    shape components), but will NOT spawn unless one is found. *
	-->ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding
	** Actor will fail to spawn. *
	-->ESpawnActorCollisionHandlingMethod::DontSpawnIfColliding
    */
    template <typename bpType>
    static FORCEINLINE bpType* SpawnBP(UWorld* theWorld,
                                       UClass* theBP,
                                       const FVector& loc,
                                       const FRotator& rot,
                                       const ESpawnActorCollisionHandlingMethod colHandMethod
                                                            = ESpawnActorCollisionHandlingMethod::AlwaysSpawn,
                                       AActor* owner = NULL,
                                       APawn* instigator = NULL) {
        if(!theWorld) return NULL;
        if(!theBP) return NULL;
        //~~
        FActorSpawnParameters spawnInfo;
        spawnInfo.SpawnCollisionHandlingOverride = colHandMethod;
        spawnInfo.Owner 				= owner;
        spawnInfo.Instigator			= instigator;
        spawnInfo.bDeferConstruction 	= false;

        return theWorld->SpawnActor<bpType>(theBP, loc ,rot, spawnInfo );
    }

    static TSubclassOf<class UObject> FindOrLoadBluePrintClass(const TCHAR* path);

    static void MakeActorLookAt(AActor *actorToRotate, const AActor &targetActor);

    static FLinearColor HSLToRGB(const double &h, const double &s, const double &l);
    static FLinearColor GenerateColorFromPreset(const int &i);
};
