This file program will generate procedural cities; the data that is dumped can be find in line 19344 in js/bundle.js

The program will generate 2 files

GeneratedCityNodes.txt and GeneratedCitySegments.txt


GeneratedCityNodes structure is:
    1rst line:
        numberOfNodes
    Rest of lines:
        NodeID PositionX PositionY


GeneratedCitySegments structure is:
    1rst line:
        NumberOfSegments LargestSegmentDistance
    Rest of lines:
        StartNode EndNode DistanceOfSegment SegmentCapacity SegmentMaxSpeed


NOTES: 
SegmentCapacity goes from [0 - 1], the closest to 1 the more capacity
SegmentSpeed    goes from [0 - 1], the closest to 1 the more speed over all the nodes

_____ALSO THE WEB-APP SHOULD BE OPENED IN CHROME____

https://github.com/t-mw/citygen/tree/master/src
